This particular project is used in blog post:
http://happilyblogging.wordpress.com/2016/05/19/using-netbeans-for-typescript/



# The original project was: Angular 2 Beta Boilerplate

All the below details applies to the original project.

## Description
This repository acts as a very simple Angular 2 Beta Boilerplate with which you can get started developing Angular 2 immediately.
It is derived from the official Angular 2 Documentation which can be found [here](https://angular.io/docs/ts/latest/quickstart.html).
## Usage
Follow the following steps and you're good to go! Important: Typescript and npm has to be installed on your machine!

1: Clone repo
```
git clone https://github.com/mschwarzmueller/angular-2-beta-boilerplate.git
```
2: Install packages
```
npm install
```
3: Start server (includes auto refreshing) and gulp watcher
```
npm start
```
