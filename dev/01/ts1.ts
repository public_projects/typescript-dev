class MyStarterTypeScriptTemplate {
    element: HTMLElement;
    span1: HTMLElement;
    paragraph1: HTMLParagraphElement;

    constructor(element: HTMLElement) {
        // 1. define the main element
        this.element = element;

        // example of directly assigning value into html page.
        this.element.innerHTML += "<h1>Meaningless text in header 1: </h1>";

        // 2. define a sub elements
        this.span1 = document.createElement('span');
        this.paragraph1 = document.createElement('p');

        // 3. Change the content of the sub elements.
        this.span1.innerHTML = 'Text displayed into span!';
        this.paragraph1.innerHTML = 'Text displayed into paragraph!';

        // 4. attach the sub element to the main element.
        this.element.appendChild(this.span1);
        this.element.appendChild(this.paragraph1);
    }

    /**
     * A simple method!
     */
    toString() {
        return 'Hello';
    }
}

window.onload = () => {

    // defining a el variable with the element from the html page
    let el: HTMLElement = document.getElementById('content');

    // Initializing class
    let myTemplate = new MyStarterTypeScriptTemplate(el);

    // Calling method of the object myTemplate
    let toStringValue = myTemplate.toString();

    // displaying some values to different output options.
    console.log("[console] "+toStringValue);
    window.alert("[alert] "+toStringValue);
};

